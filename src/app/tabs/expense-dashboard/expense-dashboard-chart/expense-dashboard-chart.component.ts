import { Component, Input, OnChanges, SimpleChanges, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { IExpenseDashboard, IExpenseDashboardCategory } from '@expenseapp/tabs/store/models/expense-dashboard.model';
import * as Chart from 'chart.js';

@Component({
  selector: 'app-expense-dashboard-chart',
  templateUrl: './expense-dashboard-chart.component.html',
  styleUrls: ['./expense-dashboard-chart.component.scss'],
})
export class ExpenseDashboardChartComponent implements OnChanges, AfterViewInit {
  @ViewChild('myChart', { static: false, read: ElementRef }) canvasChart: ElementRef | null;

  @Input() dashboard: IExpenseDashboard;
  @Input() hasError: boolean;

  public dashboardChart: Chart;

  ngAfterViewInit(): void {
    if (!this.canvasChart) {
      return;
    }
    this.dashboardChart = new Chart(this.canvasChart.nativeElement.getContext('2d'), {
      type: 'pie',
      data: {
        labels: [],
        datasets: [{
          label: '# of Votes',
          data: [],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        animation: {
          duration: 2000
        }
      }
    });

    if (this.dashboard.categories.length > 0) {
      this.setChartData(this.dashboard);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('dashboard' in changes && this.dashboardChart) {
      this.setChartData(changes.dashboard.currentValue);
    }
  }

  private setChartData(dashboard: IExpenseDashboard): void {
    const chartData = {
      count: [],
      name: []
    }
    dashboard.categories.forEach(category => {
      chartData.count.push(category.count);
      chartData.name.push(category.name);
    });
    this.dashboardChart.data.labels = chartData.name.length > 0 ? chartData.name : ['No data'];
    this.dashboardChart.data.datasets[0].data = chartData.count.length > 0 ? chartData.count : [0];
    this.dashboardChart.update();
  }
}
