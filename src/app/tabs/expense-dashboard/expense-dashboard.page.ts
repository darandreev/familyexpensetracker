import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { IExpenseDashboard } from '../store/models/expense-dashboard.model';
import { IUser } from '@expenseapp/models/user.model';
import { UserFacade } from '../store/facade/user.facade';
import { ExpenseFacade } from '../store/facade/expense.facade';

@Component({
  selector: 'app-expense-dashboard',
  templateUrl: 'expense-dashboard.page.html',
  styleUrls: ['expense-dashboard.page.scss'],
})
export class ExpenseDashboardPage {
  public dashboard$: Observable<IExpenseDashboard> = this.expenseFacade
    .dashboard$;
  public user$: Observable<IUser> = this.userFacade.user$;
  public hasError$: Observable<boolean> = this.expenseFacade.hasDashboardError$;

  constructor(
    private userFacade: UserFacade,
    private expenseFacade: ExpenseFacade
  ) {}

  ionViewWillEnter() {
    this.expenseFacade.getExpenseDashboardData('month');
    this.userFacade.getUserData();
  }

  amountChanged(amount: number): void {
    this.userFacade.updateUserAmount(amount);
  }
}
