import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExpenseDashboardPage } from './expense-dashboard.page';
import { ExpenseDashboardChartComponent } from './expense-dashboard-chart/expense-dashboard-chart.component';
import { SharedModule } from '@expenseapp/shared/shared.module';
import { ExpenseDashboardBudgetComponent } from './expense-dashboard-budget/expense-dashboard-budget.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: ExpenseDashboardPage }]),
    SharedModule
  ],
  declarations: [ExpenseDashboardPage, ExpenseDashboardChartComponent, ExpenseDashboardBudgetComponent],
})
export class ExpenseDashboardPageModule { }
