import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IState, expenseName } from '../reducers';
import { getCategoriesList } from './category.selectors';
import { getExpenses, getExpensesLoaded, getExpensesError } from './expense-list.selector';
import { getDashboardData, getDashboardError } from './expense-dashboard.selector';

const getExpenseModule = createFeatureSelector<IState>(expenseName);

const getCategoriesListSelector = createSelector(
  getExpenseModule,
  state => state.categoryList
);

const getExpensesListSelector = createSelector(
  getExpenseModule,
  state => state.expenseList
);

const getExpenseDashboardSelector = createSelector(
  getExpenseModule,
  state => state.expenseDashboard
)

export const getCategoriesListSelect = createSelector(
  getCategoriesListSelector,
  getCategoriesList
);

export const getExpenseListSelect = createSelector(
  getExpensesListSelector,
  getExpenses
);

export const getExpenseListSelectLoaded = createSelector(
  getExpensesListSelector,
  getExpensesLoaded
);

export const getExpenseListSelectError = createSelector(
  getExpensesListSelector,
  getExpensesError
);

export const getExpenseDashboardSelect = createSelector(
  getExpenseDashboardSelector,
  getDashboardData
)

export const getExpenseDashboardErrorSelect = createSelector(
  getExpenseDashboardSelector,
  getDashboardError
)
