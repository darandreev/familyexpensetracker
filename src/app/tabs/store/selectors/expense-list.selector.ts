import { IExpenseListState } from '../reducers/expense-list.reducer';

export const getExpenses = (state: IExpenseListState) => state.expenses;
export const getExpensesLoaded = (state: IExpenseListState) => state.isLoaded;
export const getExpensesError = (state: IExpenseListState) => state.error;