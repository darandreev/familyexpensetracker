import { IExpenseDashboardState } from '../reducers/expense-dashboard.reducer';

export const getDashboardData = (state: IExpenseDashboardState) => state.dashboard;
export const getDashboardError = (state: IExpenseDashboardState) => state.error;