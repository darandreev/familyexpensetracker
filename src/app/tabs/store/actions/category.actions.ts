import { createAction, props } from '@ngrx/store';
import { IExpenseCategory } from '../models/category.model';
import { actionType } from './type';

export const getCategories = createAction(
  actionType('[Expense_Add_Tab] Get categories')
);

export const getCategoriesSuccess = createAction(
  actionType('[Expense_Add_Tab] Get categories successfully'),
  props<{ categories: IExpenseCategory[] }>()
);

export const getCategoriesFailed = createAction(
  actionType('[Expense_Add_Tab] Get categories failed'),
  (error: Error) => ({ error })
);
