import { createAction, props } from '@ngrx/store';
import { IExpense } from '../../../models/expense.model';
import { actionType } from './type';

export const addExpense = createAction(
  actionType('[Expense_Add_Tab] Add expense'),
  props<{ expense: IExpense; userId: string }>()
);

export const addExpenseSuccess = createAction(
  actionType('[Expense_Add_Tab] Add expense successfully'),
  props<{ expense: IExpense }>()
);

export const addExpenseFailed = createAction(
  actionType('[Expense_Add_Tab] Add expense failed'),
  (error: Error) => ({ error })
);
