import { TestBed } from '@angular/core/testing';

import { ExpenseDashboardService } from './expense-dashboard.service';

describe('ExpenseDashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExpenseDashboardService = TestBed.get(ExpenseDashboardService);
    expect(service).toBeTruthy();
  });
});
