import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IExpenseCategory } from '../models/category.model';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  public getAllCategories(): Observable<IExpenseCategory[]> {
    return this.http.get<IExpenseCategory[]>(`${environment.API_URL}category`);
  }
}
