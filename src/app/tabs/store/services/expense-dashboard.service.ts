import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IExpenseDashboard } from '../models/expense-dashboard.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExpenseDashboardService {

  constructor(private http: HttpClient) { }

  public getExpenseDashboard(userId: string, date: string): Observable<IExpenseDashboard> {
    const params = {
      params: new HttpParams().set('userId', userId).set('date', date)
    }
    return this.http.get<IExpenseDashboard>(`${environment.API_URL}expense/categories`, params);
  }
}
