import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IState } from '../reducers';
import { addExpense } from '../actions/expense.actions';
import { IExpense, IExpenseResponse } from '../../../models/expense.model';
import { UserService } from '../../../core/services/user.service';
import { getCategories } from '../actions/category.actions';
import { Observable } from 'rxjs';
import { IExpenseCategory } from '../models/category.model';
import {
  getCategoriesListSelect,
  getExpenseDashboardSelect,
  getExpenseListSelect,
  getExpenseListSelectLoaded,
  getExpenseListSelectError,
  getExpenseDashboardErrorSelect
} from '../selectors';
import { getExpenseDashboardData } from '../actions/expense-dashboard.actions';
import { IExpenseDashboard } from '../models/expense-dashboard.model';
import { loadExpenseList, deleteExpense, resetExpenseList } from '../actions/expense-list.action';

@Injectable()
export class ExpenseFacade {
  categories$: Observable<IExpenseCategory[]> = this.store.pipe(select(getCategoriesListSelect));
  dashboard$: Observable<IExpenseDashboard> = this.store.pipe(select(getExpenseDashboardSelect));
  expenses$: Observable<IExpenseResponse> = this.store.pipe(select(getExpenseListSelect));
  isLoaded$: Observable<boolean> = this.store.pipe(select(getExpenseListSelectLoaded));
  hasError$: Observable<boolean> = this.store.pipe(select(getExpenseListSelectError));
  hasDashboardError$: Observable<boolean> = this.store.pipe(select(getExpenseDashboardErrorSelect));

  constructor(private store: Store<IState>, private userService: UserService) {}

  public addExpense(expense: IExpense): void {
    this.store.dispatch(addExpense({ expense, userId: this.userService.userId }));
  }

  public getCategories(): void {
    this.store.dispatch(getCategories());
  }

  public getExpenseDashboardData(date: string): void {
    this.store.dispatch(getExpenseDashboardData({ userId: this.userService.userId, date }));
  }

  public getExpenseList(segment: string, skip: number, limit: number): void {
    this.store.dispatch(
      loadExpenseList({
        date: segment,
        userId: this.userService.userId,
        skip,
        limit
      })
    );
  }

  public resetExpenseList(): void {
    this.store.dispatch(resetExpenseList());
  }

  public deleteExpense(expenseId: string): void {
    this.store.dispatch(deleteExpense({ expenseId }));
  }
}
