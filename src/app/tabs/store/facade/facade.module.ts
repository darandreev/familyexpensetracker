import { NgModule } from '@angular/core';
import { ExpenseFacade } from './expense.facade';
import { UserFacade } from './user.facade';

@NgModule({
    providers: [UserFacade, ExpenseFacade]
})
export class FacadeModule { }
