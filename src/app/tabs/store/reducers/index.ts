import { ICategoryListState, categoriesListReducer } from './category.reducers';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../../../environments/environment.prod';
import { IExpenseAddState, expenseAddReducer } from './expense.reducer';
import { IExpenseListState, expenseListReducer } from './expense-list.reducer';
import { IExpenseDashboardState, expenseDashboardReducer } from './expense-dashboard.reducer';

export const expenseName = 'expense';

export interface IState {
  readonly categoryList: ICategoryListState;
  readonly expenseAdd: IExpenseAddState;
  readonly expenseList: IExpenseListState;
  readonly expenseDashboard: IExpenseDashboardState;
}

export const expenseReducerMap: ActionReducerMap<IState> = {
  categoryList: categoriesListReducer,
  expenseAdd: expenseAddReducer,
  expenseList: expenseListReducer,
  expenseDashboard: expenseDashboardReducer
};

export const metaReducers: MetaReducer<IState>[] = !environment.production
  ? []
  : [];
