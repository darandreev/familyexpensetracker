import { createReducer, on, Action } from '@ngrx/store';
import { IExpenseDashboard } from '../models/expense-dashboard.model';
import {
  getExpenseDashboardData,
  getExpenseDashboardDataSuccess,
  getExpenseDashboardDataFailed,
} from '../actions/expense-dashboard.actions';

export interface IExpenseDashboardState {
  dashboard: IExpenseDashboard;
  isLoaded?: boolean;
  error?: boolean;
}

const initialExpenseDashboardState: IExpenseDashboardState = {
  dashboard: {
    categories: [],
    totalMonthlyAmount: 0
  },
  isLoaded: false,
  error: false
};

const reducer = createReducer<IExpenseDashboardState>(
  initialExpenseDashboardState,
  on(getExpenseDashboardData, state => ({ ...state })),
  on(getExpenseDashboardDataSuccess, (state, { dashboard }) => ({
    ...state,
    dashboard,
    isLoaded: true,
    error: false
  })),
  on(getExpenseDashboardDataFailed, state => ({
    ...state,
    isLoaded: false,
    error: true
  })),
);

export function expenseDashboardReducer(state: IExpenseDashboardState, action: Action) {
  return reducer(state, action);
}
