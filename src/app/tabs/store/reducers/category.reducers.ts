import { IExpenseCategory } from '../models/category.model';
import { createReducer, on, Action } from '@ngrx/store';
import {
  getCategories,
  getCategoriesSuccess
} from '../actions/category.actions';

export interface ICategoryListState {
  categories: IExpenseCategory[];
  isLoaded?: boolean;
}

const initialState: ICategoryListState = {
  categories: []
};

const reducer = createReducer<ICategoryListState>(
  initialState,
  on(getCategories, state => ({ ...state })),
  on(getCategoriesSuccess, (state, { categories }) => ({
    ...state,
    categories
  }))
);

export function categoriesListReducer(
  state: ICategoryListState | undefined,
  action: Action
) {
  return reducer(state, action);
}
