import { IExpense } from '../../../models/expense.model';
import { createReducer, on, Action } from '@ngrx/store';
import {
  addExpenseSuccess,
  addExpenseFailed
} from '../actions/expense.actions';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

export interface IExpenseAddState extends EntityState<IExpense> {
  expenses: IExpense[];
  isLoaded?: boolean;
  errorMessage?: string;
}

const adapter: EntityAdapter<IExpense> = createEntityAdapter<IExpense>({
  selectId: (expense) => expense.id,
  sortComparer: false
});

const initialState: IExpenseAddState = adapter.getInitialState({
  expenses: [],
  isLoaded: false
});

const reducer = createReducer<IExpenseAddState>(
  initialState,
  on(addExpenseSuccess, (state, { expense }) => ({
    ...state,
    expenses: [expense, ...state.expenses],
    isLoaded: true
  })),
  on(addExpenseFailed, (state, { error }) => ({
    ...state,
    expenses: [],
    isLoaded: false,
    errorMessage: error.message
  }))
);

export function expenseAddReducer(
  state: IExpenseAddState | undefined,
  action: Action
) {
  return reducer(state, action);
}
