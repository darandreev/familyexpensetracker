import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  addExpense,
  addExpenseSuccess,
  addExpenseFailed
} from '../actions/expense.actions';
import { mergeMap, map, catchError, tap } from 'rxjs/operators';
import { ExpenseService } from '../services/expense.service';
import { HelperService, ToastColorEnum } from '@expenseapp/core/services/helper.service';

@Injectable()
export class ExpenseEffects {
  constructor(
    private actions$: Actions,
    private expenseService: ExpenseService,
    private helperService: HelperService
  ) { }

  addExpense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addExpense),
      mergeMap(({ expense, userId }) =>
        this.expenseService.addExpense(expense, userId).pipe(
          map(exp => addExpenseSuccess({ expense: exp })),
          catchError(error => [addExpenseFailed(error)])
        )
      )
    )
  );

  addExpenseSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addExpenseSuccess),
      tap(() => this.helperService.presentToast('Uspesno dodato!', ToastColorEnum.SUCCESS)
      )
    ),
    { dispatch: false }
  );
}
