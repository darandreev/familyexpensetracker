import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  getCategories,
  getCategoriesSuccess,
  getCategoriesFailed
} from '../actions/category.actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { CategoryService } from '../services/category.service';

@Injectable()
export class CategoryEffects {
  constructor(
    private actions$: Actions,
    private categoryService: CategoryService
  ) {}

  getCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCategories),
      switchMap(() =>
        this.categoryService.getAllCategories().pipe(
          map(categories => getCategoriesSuccess({ categories })),
          catchError(error => [getCategoriesFailed(error)])
        )
      )
    )
  );
}
