import { Injectable } from '@angular/core';
import { ExpenseService } from '../services/expense.service';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import {
  loadExpenseList,
  loadExpenseListSuccess,
  loadExpenseListFailiure,
  deleteExpense,
  deleteExpenseSuccess,
  deleteExpenseFailure
} from '../actions/expense-list.action';
import { switchMap, map, catchError } from 'rxjs/operators';

@Injectable()
export class ExpenseListEffect {
  constructor(
    private actions$: Actions,
    private expenseService: ExpenseService
  ) { }

  loadExpenseList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadExpenseList),
      switchMap(({ date, userId, skip, limit }) =>
        this.expenseService.getExpenses(userId, skip, limit, date).pipe(
          map(expenses => loadExpenseListSuccess({ expenses })),
          catchError(error => [loadExpenseListFailiure(error)])
        )
      )
    )
  );

  deleteExpense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteExpense),
      switchMap(({ expenseId }) =>
        this.expenseService.deleteExpense(expenseId).pipe(
          map(expense => deleteExpenseSuccess({ expense })),
          catchError(error => [deleteExpenseFailure(error)])
        )
      )
    )
  )
}
