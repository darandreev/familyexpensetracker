import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExpenseAddPage } from './page/expense-add.page';
import { FormComponent } from './form/form.component';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@expenseapp/shared/shared.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: ExpenseAddPage }]),
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [ExpenseAddPage, FormComponent],
})
export class ExpenseAddModule { }
