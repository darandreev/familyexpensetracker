import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { IExpenseCategory } from '../../store/models/category.model';
import { IUser } from '../../../models/user.model';
import { IExpense } from '@expenseapp/models/expense.model';
import { FormComponent } from '../form/form.component';
import { ExpenseFacade } from '../../store/facade/expense.facade';
import { UserFacade } from '../../store/facade/user.facade';

@Component({
  selector: 'expense-add-tab',
  templateUrl: 'expense-add.page.html',
  styleUrls: ['expense-add.page.scss'],
})
export class ExpenseAddPage {
  @ViewChild(FormComponent, { static: false })
  expenseFormComponent: FormComponent;

  user$: Observable<IUser> = this.userFacade.user$;
  categories$: Observable<IExpenseCategory[]> = this.expenseFacade.categories$;

  constructor(
    private expenseFacade: ExpenseFacade,
    private userFacade: UserFacade
  ) {}

  ionViewWillEnter() {
    this.expenseFacade.getCategories();
  }

  onSubmit(expense: IExpense): void {
    this.expenseFacade.addExpense(expense);
    this.expenseFormComponent.reset();
  }
}
