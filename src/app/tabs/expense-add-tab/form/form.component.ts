import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  OnInit,
} from '@angular/core';
import { IExpenseCategory } from '../../store/models/category.model';
import { IExpense } from '../../../models/expense.model';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
  @Input() public categories: IExpenseCategory[];
  @Output() public submitted = new EventEmitter<IExpense>();

  public expenseForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.createFormGroup();
  }

  public onSubmit(form: FormGroup): void {
    if (form.valid) {
      const expense: IExpense = form.value;
      this.submitted.emit(expense);
    }
  }

  private createFormGroup(): void {
    this.expenseForm = this.fb.group({
      title: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      categoryId: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      date: new FormControl(new Date().toISOString()),
    });
  }

  public reset(): void {
    this.expenseForm.reset({
      date: new FormControl(new Date().toISOString()),
    });
  }
}
