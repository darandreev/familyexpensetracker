import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabsPageRoutingModule } from './tabs-routing.module';
import { TabsPage } from './page/tabs.page';
import { StoreModule } from '@ngrx/store';
import { expenseName, expenseReducerMap } from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { CategoryEffects } from './store/effects/category.effects';
import { ExpenseEffects } from './store/effects/expense.effects';
import { ExpenseHistoryPageModule } from './expense-history-tab/expense-history.module';
import { ExpenseAddModule } from './expense-add-tab/expense-add.module';
import { ExpenseListEffect } from './store/effects/expense-list.effects';
import { ExpenseDashboardEffects } from './store/effects/expense-dashboard.effects';
import { SettingsModule } from './settings/settings.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    ExpenseAddModule,
    ExpenseHistoryPageModule,
    SettingsModule,
    StoreModule.forFeature(expenseName, expenseReducerMap),
    EffectsModule.forFeature([
      CategoryEffects,
      ExpenseEffects,
      ExpenseListEffect,
      ExpenseDashboardEffects
    ])
  ],
  declarations: [TabsPage]
})
export class TabsPageModule { }
