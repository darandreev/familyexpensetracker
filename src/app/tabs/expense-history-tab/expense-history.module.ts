import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExpenseHistoryPage } from './expense-history.page';
import { ExpenseSegmentComponent } from './expense-segment/expense-segment.component';
import { ExpenseListComponent } from './expense-list/expense-list.component';
import { SharedModule } from '@expenseapp/shared/shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: ExpenseHistoryPage }]),
    SharedModule,
    InfiniteScrollModule
  ],
  declarations: [
    ExpenseHistoryPage,
    ExpenseSegmentComponent,
    ExpenseListComponent
  ]
})
export class ExpenseHistoryPageModule { }
