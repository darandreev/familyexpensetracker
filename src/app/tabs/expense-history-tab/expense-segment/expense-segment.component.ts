import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input,
} from '@angular/core';

@Component({
  selector: 'app-expense-segment',
  templateUrl: './expense-segment.component.html',
  styleUrls: ['./expense-segment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExpenseSegmentComponent {
  @Output() segmentChange = new EventEmitter<string>();
  @Input() segment: string;

  segmentChanged(event: CustomEvent) {
    this.segmentChange.emit(event.detail.value);
  }
}
