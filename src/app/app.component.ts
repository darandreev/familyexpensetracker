import { Component, NgZone } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Store } from '@ngrx/store';
import { isLoggedInAction } from './login/store/actions';
import { IState } from './login/store/reducers';
import { HeaderService } from './core/services/header.service';
import { Observable } from 'rxjs';
import { Network } from '@ionic-native/network/ngx';
import { merge } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

export enum NetworkStatus {
  CONNECTED = 'Network connected',
  DISCONECTED = 'Network disconnected'
}
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  title$: Observable<string>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private store: Store<IState>,
    private headerService: HeaderService,
    private network: Network,
    private ngZone: NgZone,
    private snackBar: MatSnackBar
  ) {
    this.initializeApp();
    this.title$ = this.headerService.headerTitle;
  }

  private initializeApp(): void {
    this.platform.ready().then(() => {
      this.ngZone.runOutsideAngular(() => {
        merge(
          this.network.onDisconnect().pipe(mapTo(NetworkStatus.DISCONECTED)),
          this.network.onConnect().pipe(mapTo(NetworkStatus.CONNECTED))
        ).subscribe((status: NetworkStatus) => {
          this.snackBar.open(status, '', {
            duration: 2000,
            verticalPosition: 'bottom'
          });
        });
      });
      this.store.dispatch(isLoggedInAction());
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
