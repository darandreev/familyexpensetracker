export interface IExpense {
  id: string;
  title: string;
  amount: number;
  date: Date | null;
  description: string;
  total: string;
  categoryName?: string;
  categoryId?: string;
  userId?: string;
  isDeleted?: boolean;
}
export interface IExpenseResponse {
  result: Array<IExpense>;
  total: number;
}
export interface IExpenseChart {
  labels: Array<any>,
  datasets: Array<any>
}
