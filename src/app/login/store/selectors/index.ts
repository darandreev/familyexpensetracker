import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IState, userFeatureKey } from '../reducers';
import { selectUser, selectUserLoggedIn } from './user.selectors';

export const getUserModule = createFeatureSelector<IState>(userFeatureKey);

const getUserAuth = createSelector(getUserModule, state => state.authState);

export const userSelector = createSelector(getUserAuth, selectUser);
export const userLoggedInSelector = createSelector(
  getUserAuth,
  selectUser,
  selectUserLoggedIn
);
