import { IAuthState } from '../reducers/auth.reducers';

export const selectUser = (state: IAuthState) => state.user;
export const selectUserLoggedIn = (state: IAuthState) => state.isLoggedIn;
