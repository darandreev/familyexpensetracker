import { createAction, props } from '@ngrx/store';
import { ICredentials, IUser, ILoginResponse } from '../../../models/user.model';

export const loginAction = createAction(
  '[Login Page] Login',
  props<{ credentials: ICredentials }>()
);

export const loginSuccess = createAction(
  '[Login Page] Login success',
  props<{ response: ILoginResponse }>()
);

export const loginFailed = createAction(
  '[Login Page] Login failed',
  (error: Error) => ({ error })
);

export const loginRedirect = createAction('[Auth] Login Redirect');

export const isLoggedInAction = createAction('[Auth] isUserLoggedIn');

export const isLoggedInSuccess = createAction(
  '[Auth] isUserLoggedIn success',
  props<{ isLoggedIn: boolean, token: string }>()
);
export const isLoggedInError = createAction(
  '[Auth] isUserLoggedIn error',
  (error: Error) => ({ error })
);

export const logoutAction = createAction('[Auth] Logout');

export const getUserData = createAction(
  '[ExpenseDashboard] Get user data',
  props<{ userId: string }>()
)


export const getUserDataSuccess = createAction(
  '[ExpenseDashboard] Get user data success',
  props<{ user: IUser }>()
)

export const getUserDataFailed = createAction(
  '[ExpenseDashboard] Get user data failed',
  (error: Error) => ({ error })
)
