import * as fromAuth from './auth.reducers';
import * as fromLogin from './login.reducers';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../../../environments/environment.prod';

export const userFeatureKey = 'userFeatureKey';

export interface IState {
  readonly authState: fromAuth.IAuthState;
  // readonly loginPageState: fromLogin.State;
}

export const loginAuthReducerMap: ActionReducerMap<IState> = {
  authState: fromAuth.authReducer,
  // oginPageState: fromLogin.loginPageReducer,
};

export const metaReducers: MetaReducer<IState>[] = !environment.production
  ? []
  : [];
