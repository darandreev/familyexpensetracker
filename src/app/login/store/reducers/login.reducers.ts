import { createReducer, on, Action } from '@ngrx/store';
import {
  loginAction,
  loginSuccess,
  loginFailed
} from '../actions/login.actions';

export const loginPageFeatureKey = 'loginPageFeatureKey';

export interface State {
  error: Error | null;
  pending: boolean;
}

export const initialState: State = {
  error: null,
  pending: false
};

export const reducer = createReducer(
  initialState,
  on(loginAction, state => ({
    ...state,
    error: null,
    pending: true
  })),

  on(loginSuccess, state => ({
    ...state,
    error: null,
    pending: false
  })),

  on(loginFailed, (state, { error }) => ({
    ...state,
    error,
    pending: false
  }))
);

export function loginPageReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}

export const getError = (state: State) => state.error;
export const getPending = (state: State) => state.pending;
