import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { IState } from '../reducers';
import { Observable } from 'rxjs';
import { userLoggedInSelector } from '../selectors';
import { map, take } from 'rxjs/operators';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private store: Store<IState>, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(userLoggedInSelector),
      map(token => {
        if (!token) {
          this.router.navigate(['/login']);
          return false;
        }
        return true;
      }),
      take(1)
    );
  }
}
