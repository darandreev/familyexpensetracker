import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserService } from '@expenseapp/core/services/user.service';
import { updateUserAmout, updateUserAmoutSuccess, updateUserAmoutFailed } from '../actions';
import { switchMap, map, catchError } from 'rxjs/operators';

@Injectable()
export class UserEffects {
    constructor(private actions$: Actions, private userService: UserService) {}

    updateUserAmount$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updateUserAmout),
            switchMap(({userId, amount}) => this.userService.updateUserAmount(userId, amount).pipe(
                map(user => updateUserAmoutSuccess({ user })),
                catchError(error => [updateUserAmoutFailed(error)])
            ))
        )
    );
}