import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  loginAction,
  loginSuccess,
  loginFailed,
  loginRedirect,
  logoutAction,
  isLoggedInAction,
  isLoggedInSuccess,
  isLoggedInError,
  getUserDataSuccess,
  getUserDataFailed,
  getUserData
} from '../actions/login.actions';
import { exhaustMap, map, catchError, tap, switchMap } from 'rxjs/operators';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { UserService } from '@expenseapp/core/services/user.service';
import { NavController } from '@ionic/angular';

@Injectable()
export class LoginEffects {
  constructor(
    private actions$: Actions,
    private loginService: LoginService,
    private userService: UserService,
    private router: Router,
    private navCtrl: NavController
  ) { }

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginAction),
      exhaustMap(({ credentials }) =>
        this.loginService.login(credentials).pipe(
          map(response => loginSuccess({ response })),
          catchError(error => [loginFailed(error)])
        )
      )
    )
  );

  isLoggedIn$ = createEffect(() =>
    this.actions$.pipe(
      ofType(isLoggedInAction),
      exhaustMap(() =>
        this.userService.isAuthenticated.pipe(
          map(({ isLoggedIn, token }) => {
            if (isLoggedIn) {
              return isLoggedInSuccess({ isLoggedIn, token });
            }
            return isLoggedInError(new Error('No token provided'));
          }),
          catchError(error => [isLoggedInError(error)])
        )
      )
    )
  );


  getUserData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getUserData),
      switchMap(({ userId }) =>
        this.userService.getUserData(userId).pipe(
          map(user => getUserDataSuccess({ user })),
          catchError(error => [getUserDataFailed(error)])
        ))
    )
  )

  isLoggedInSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(isLoggedInSuccess),
      tap(({ isLoggedIn, token }) => {
        if (isLoggedIn && token) {
          this.router.navigate(['tabs']);
        }
      })),
    { dispatch: false }
  );

  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginSuccess),
        tap(({ response }) => {
          localStorage.setItem('accessToken', response.accessToken);
          this.router.navigate(['tabs']);
        })
      ),
    { dispatch: false }
  );

  loginRedirect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginRedirect, logoutAction),
        tap(() => {
          localStorage.clear();
          this.navCtrl.navigateRoot('/login');
          this.navCtrl.setDirection('root', true);
        })
      ),
    { dispatch: false }
  );
}
