import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { IState } from '../store/reducers';
import { ICredentials } from '../../models/user.model';
import { loginAction } from '@expenseapp/login/store/actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage {
  constructor(private store: Store<IState>) { }

  onSubmit(credentials: ICredentials): void {
    this.store.dispatch(loginAction({ credentials }));
  }
}
