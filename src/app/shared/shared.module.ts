import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { SidebarMenuComponent } from './sidebar-menu/sidebar-menu.component';
import { SidebarMenuDetailsComponent } from './sidebar-menu/details/details.component';
import { FacadeModule } from '../tabs/store/facade/facade.module';
import { FormsModule } from '@angular/forms';
import { HeaderFilterComponent } from './header/header-filter/header-filter.component';

@NgModule({
  declarations: [HeaderComponent, SidebarMenuComponent, SidebarMenuDetailsComponent, HeaderFilterComponent],
  imports: [
    IonicModule,
    CommonModule,
    FacadeModule,
    FormsModule
  ],
  exports: [HeaderComponent, SidebarMenuComponent, SidebarMenuDetailsComponent],
  entryComponents: [HeaderFilterComponent]
})
export class SharedModule { }
