import { Component, OnInit } from '@angular/core';
import { IUser } from '@expenseapp/models/user.model';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { userSelector } from '@expenseapp/login/store/selectors';
import { IState } from '@expenseapp/tabs/store/reducers';

@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.scss'],
})
export class SidebarMenuComponent {
  user$: Observable<IUser> = this.store.pipe(select(userSelector));;

  constructor(private store: Store<IState>) { }
}
