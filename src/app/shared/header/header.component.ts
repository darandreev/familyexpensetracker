import {
  Component,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
} from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromLogin from '@expenseapp/login/store/reducers';
import { logoutAction } from '@expenseapp/login/store/actions';
import { PopoverController } from '@ionic/angular';
import { HeaderFilterComponent } from './header-filter/header-filter.component';
import { ExpenseFacade } from '../../tabs/store/facade/expense.facade';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  @Input() title: string;
  @Input() showMenu: boolean = true;
  @Input() showFilter: boolean = false;

  @Output() filter: EventEmitter<string> = new EventEmitter();

  constructor(
    private store: Store<fromLogin.IState>,
    private expenseFacade: ExpenseFacade,
    private popoverController: PopoverController
  ) {}

  public logout(): void {
    this.store.dispatch(logoutAction());
  }

  async presentPopover(event: MouseEvent) {
    const popover = await this.popoverController.create({
      component: HeaderFilterComponent,
      event,
      translucent: true,
      showBackdrop: false,
    });
    await popover.present();
    const { filter = 'month' } = (await popover.onWillDismiss()).data || '';
    this.expenseFacade.getExpenseDashboardData(filter.toLowerCase());
  }
}
