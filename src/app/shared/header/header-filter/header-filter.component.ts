import { Component, ChangeDetectionStrategy } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-header-filter',
  templateUrl: './header-filter.component.html',
  styleUrls: ['./header-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderFilterComponent {
  public readonly filters: string[] = ['Month', 'Week', 'Today', 'All'];

  constructor(private popover: PopoverController) { }

  handleClick(filter: string): void {
    this.popover.dismiss({ filter });
  }
}
