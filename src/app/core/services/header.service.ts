import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class HeaderService {
  constructor(private titleService: Title, private router: Router) {}

  public get headerTitle(): Observable<string> {
    return this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map((nav: NavigationEnd) => {
        const title = this.defineTitle(nav.urlAfterRedirects.split('/').pop());
        this.titleService.setTitle(title);
        return title;
      })
    );
  }

  private defineTitle(title: string): string {
    let headerTitle: string = '';
    switch (title) {
      case 'home':
        headerTitle = 'Dashboard';
        break;
      case 'expense':
        headerTitle = 'Add expense';
        break;
      case 'history':
        headerTitle = 'Transactions';
        break;
      case 'settings':
        headerTitle = 'Settings';
        break;
      default:
        headerTitle = 'Login';
        break;
    }

    return headerTitle;
  }
}
