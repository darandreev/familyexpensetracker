import { Injectable } from '@angular/core';
import { IUser } from '@expenseapp/models/user.model';
import { Observable, of } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IState } from '@expenseapp/tabs/store/reducers';
import { userLoggedInSelector } from '@expenseapp/login/store/selectors';
import * as jwtDecode from 'jwt-decode';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private store: Store<IState>, private http: HttpClient) {
  }

  public get userId(): string {
    const token = this.token;
    if (!token) {
      return;
    }

    return (jwtDecode<IUser>(token)).id;
  }

  public getUserData(userId: string): Observable<IUser> {
    const params = {
      params: new HttpParams().set('userId', userId)
    };
    return this.http.get<IUser>(`${environment.API_URL}user`, params);
  }

  public updateUserAmount(userId: string, amount: number): Observable<IUser> {
    const params = {
      params: new HttpParams().set('userId', userId).set('amount', amount.toString())
    };

    return this.http.patch<IUser>(`${environment.API_URL}user`, null, params);
  }

  public isUserLoggedIn(): Observable<boolean> {
    return this.store.pipe(select(userLoggedInSelector));
  }

  public get token(): string {
    return localStorage.getItem('accessToken');
  }


  public get isAuthenticated(): Observable<{ isLoggedIn: boolean; token: string }> {
    return of({
      isLoggedIn: !this.isTokenExpired(this.token),
      token: this.token
    });
  }

  private getTokenExpirationDate(token: string): Date {
    const decoded = jwtDecode<any>(token);

    if (!decoded.exp) {
      return;
    }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) {
      return true;
    }
    const date = this.getTokenExpirationDate(token);
    if (!date) {
      return false;
    }

    return !(date.getTime() > new Date().getTime());
  }

}
