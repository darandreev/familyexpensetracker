import { Injectable, EventEmitter } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular';

export enum ToastColorEnum {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  DANGER = 'danger',
  SUCCESS = 'success',
}
@Injectable({
  providedIn: 'root',
})
export class HelperService {
  constructor(
    private toastController: ToastController,
    private alertController: AlertController
  ) {}

  public async presentToast(message: string, color: ToastColorEnum) {
    const toast = await this.toastController.create({
      message,
      duration: 5000,
      color,
      buttons: [
        {
          text: 'Done',
          role: 'cancel',
        },
      ],
    });
    return toast.present();
  }

  public async presentInputAlert<T>(
    header: string,
    name: string,
    placeholder: string,
    emitter: EventEmitter<T>
  ) {
    const alert = await this.alertController.create({
      header,
      inputs: [
        {
          name,
          type: 'number',
          placeholder,
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Ok',
          handler: (value) => {
            emitter.emit(value.amount);
          },
        },
      ],
    });

    await alert.present();
  }

  public async presentAlert(
    header: string,
    message: string,
    callback: () => void
  ) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Ok',
          handler: () => callback(),
        },
      ],
    });

    await alert.present();
  }
}
